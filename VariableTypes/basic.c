#include <stdio.h>

typedef int uint16;

int main()
{

    uint16 car1 = 0;
    printf("Please enter number of car 1: ");
    scanf("%d", &car1);

    uint16 car2 = 0;
    printf("Please enter number of car 2: ");
    scanf("%d", &car2);

    printf("\nTotal number of cars is: %d\n", car2 + car1);

    return 0;
}
