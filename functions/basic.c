#include <stdio.h>

int depreciationCost(float fCost, int fAge);
int main()
{
    float carCost = 0.0;
    printf("\nPlease insert your car cost: ");
    scanf("%f", &carCost);

    int carAge = 0;
    printf("\nPlease insert the age of your car: ");
    scanf("%d", &carAge);

    depreciationCost(carCost, carAge);
    

    return 0;
}

int depreciationCost(float fCost, int fAge){
    float carDep = 0;

    carDep = fCost / fAge;

    printf("Your car depreciation is: %.2f", carDep);

    return 0;
}