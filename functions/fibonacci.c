#include <stdio.h>

int fibonacci(int one, int two, int elements);

int main()
{
    int timesElement = 0;
    printf("Please enter how much elements of the fibonnaci list you want displayed: ");
    scanf("%d", &timesElement);

    fibonacci(1, 1, timesElement);

    return 0;
}

int fibonacci(int one, int two, int elements)
{
    if (elements > 0)
    {
        printf("%d ", one);
        int swap = one + two;

        fibonacci(two, swap, elements-1);
    }
    else
        return 0;
}