#include <stdio.h>

int ifBrake(int distance);

int main()
{

    int dist = 0;
    ifBrake(dist);

    return 0;
}

int ifBrake(int distance)
{
    printf("Please enter the distance between the cars: ");
    scanf("%d", &distance);
    if (distance < 1)
    {
        printf("invalid distance\n");
        ifBrake(distance);
    }
    else if (distance >= 1 && distance <= 7)
    {
        printf("Please keep a distance of minimum 15 Meters");
        return 0;
    }
    else if (distance > 7 && distance < 15)
    {
        printf("For now it's ok, but make sure to keep a distance of at least 15 Meters.");
        return 0;
    }
    else
    {
        printf("I'll allow it.");
        return 0;
    }
}