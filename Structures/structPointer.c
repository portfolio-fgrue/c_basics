#include <stdio.h>

typedef struct carDet
{
    int carSpeed;
    int carAge;
    int carMileAge;
    int carCost;
} carDet;

int printData(struct carDet Car);

int main()
{
    struct carDet Car;
    struct carDet *pCar = &Car;

    printf("%-30s", "Please insert the max. speed of your car: ");
    scanf("%d", &pCar->carSpeed);

    printf("%-30s", "Please insert the age of your car: ");
    scanf("%d", &pCar->carAge);

    printf("%-30s", "Please insert the mileage of your car: ");
    scanf("%d", &pCar->carMileAge);

    printf("%-30s", "Please insert the cost of your car: ");
    scanf("%d", &pCar->carCost);

    printData(Car);
    return 0;
}

int printData(struct carDet Car)
{

    printf("%-11s|%10s\n", "DATA", "VALUE");
    printf("%22s\n","----------------------");
    printf("%-11s|%10d\n", "max. speed", Car.carSpeed);
    printf("%-11s|%10d\n", "Age", Car.carAge);
    printf("%-11s|%10d\n", "Mileage", Car.carMileAge);
    printf("%-11s|%10d\n", "Cost", Car.carCost);
    
    return 0;
}