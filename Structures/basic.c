#include <stdio.h>

struct carDet
{
    int fuelCap;
    int age;
    int cost;
    int mileage; //kilometerstand
};

int main()
{
    struct carDet Car = {20, 5, 200000, 400};

    printf("%-35s %d\n", "Your max. capacity in liters is:", Car.fuelCap);
    printf("%-35s %d\n", "The age of your car is:", Car.age);
    printf("%-35s %d\n", "Yout car costed:", Car.cost);
    printf("%-35s %d\n", "Your car has a mileage of:", Car.mileage);

    return 0;
}
