#include <stdio.h>

typedef struct EmployeeData
{
    int employeeName[20];
    int employeeID;
    int employeeDepartment[20];
    float employeeSalary;
} Employee;

int main()
{
    Employee Emps[3] = {0};

    for (int i = 0; i < 3; i++)
    {
        printf("\nPlease insert the name of Employee N°%d. Please do not use Spaces: ", i + 1);
        scanf("%s", &Emps[i].employeeName);

        printf("\nPlease insert the ID of Employee N°%d: ", i + 1);
        scanf("%i", &Emps[i].employeeID);

        printf("\nPlease insert the department of Employee N°%d: ", i + 1);
        scanf("%s", &Emps[i].employeeDepartment);
        
        printf("\nPlease insert the Salary of Employee N°%d: ", i + 1);
        scanf("%s", &Emps[i].employeeSalary);
    }

    for (int i = 0; i < 3; i++)
    {
        printf("\nEmployee N°%d's name:       %s", i + 1, Emps[i].employeeName);
        printf("\nEmployee N°%d's ID:         %d", i + 1, Emps[i].employeeID);
        printf("\nEmployee N°%d's Department: %s", i + 1, Emps[i].employeeDepartment);
        printf("\nEmployee N°%d's Salary:     %f\n", i + 1, Emps[i].employeeSalary);
    }

    return 0;
}
