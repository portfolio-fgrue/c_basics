#include <stdio.h>

int main()
{
    int carDir = 0;
    printf("You have following options to turn your car: \n1. left\n2. right\n3. reverse\nPlease input the direction in which you want to turn your car: ");
    scanf("%d", &carDir);

    int *addCarDir = &carDir; // != int* addCarDir = carDir;

    switch (*addCarDir)
    {
    case 1:
        printf("\nYour car did the turn left.");
        break;
    case 2:
        printf("\nYour car did the turn right.");
        break;

    case 3:
        printf("\nYour car did the reverse.");
        break;
    }
    return 0;
}
