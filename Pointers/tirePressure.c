#include <stdio.h>

int lowPressure(int *val, int size); //int pointer erstellen und auf den geben wir dann die adresse vom array. Die Size variable dient dazu zu schauen, wie groß das array ist

int main()
{
    int arr[4] = {0};

    for (int i = 0; i < 4; i++)
    {
        printf("Please enter the tire pressure of tire %d in psi: ", i + 1);
        scanf("%d", &arr[i]);
    }

    lowPressure(arr, 4); //arr == &arr[0]

    return 0;
}

int lowPressure(int *val, int size)
{
    for (int i = 0; i < size; i++)
    {
        if (*val < 32)
        {
            printf("Tire pressure of tire %d is to low.\n", i + 1);
            val++;
        }
        else
        {
            printf("Tire pressure of tire %d is fine.\n", i + 1);
            val++;
        }
    }
    return 0;
}