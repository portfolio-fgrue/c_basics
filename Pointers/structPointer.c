#include <stdio.h>

typedef struct carData
{
    int maxSpeed;
    int fuelCapacity;
    int numOfSeats;
} carData;

int main()
{
    struct carData Car = {200, 100, 5};

    carData *pCar = &Car;

    printf("The max car speed is: %d\n", pCar->maxSpeed);
    printf("The fuel capacity is: %d\n", pCar->fuelCapacity);
    printf("The car has %d seats.\n", pCar->numOfSeats);

    return 0;
}
