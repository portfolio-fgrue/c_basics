#include <stdio.h>

int main()
{
    printf("\nIn the following input commands use natural numbers only.\n");

    int speedInKm = 0;
    printf("\nPlease Enter your speed in Kilometers per hour: ");
    scanf("%d", &speedInKm);

    int travelTimeInH = 0;
    printf("\nPlease enter your travel time in hours: ");
    scanf("%d", &travelTimeInH);

    int fuelSpentInLitres = 0;
    printf("\nPlease enter the amount of fuel spent in litres: ");
    scanf("%d", &fuelSpentInLitres);

    int distance = speedInKm * travelTimeInH;
    int velocity = distance / travelTimeInH;
    int avFuelConsumption = distance / fuelSpentInLitres;

    printf("\n\nThe following values are natural numbers so the value is not 100 percent accurate.\n\n\n");

    printf("Your distance travelled is: %d\n", distance);
    printf("Your velocity was %d\n", velocity);
    printf("Your avarage fuel consumption was: %d\n", avFuelConsumption);

    return 0;
}