#include <stdio.h>

int getOperation(int operation);
float getFirstOperand();
float getSecondOperand();
void addition(float firstOperand, float secondOperand);
void subtraction(float firstOperand, float secondOperand);
void multiplication(float firstOperand, float secondOperand);
void division(float firstOperand, float secondOperand);

int main()
{
    float firstOperand = getFirstOperand();
    float secondOperand = getSecondOperand();
    int operation = getOperation(operation);
    int i = 0;
    

    switch (operation)
    {
    case 1:
        addition(firstOperand, secondOperand);
        break;

    case 2:
        subtraction(firstOperand, secondOperand);
        break;

    case 3:
        multiplication(firstOperand, secondOperand);
        break;

    case 4:
        division(firstOperand, secondOperand);
        break;

    default:
        i = 1;
        break;
    }

    return 0;
}

int getOperation(int operation)
{
    printf("\nOptions of calculator: addition, subtraction, multiplication and division");
    printf("Enter the following numbers for options:\n%s\n%s\n%s\n%s\nEnter: ", "1. Addition", "2. Subtraction", "3. Multiplication", "4. Division");
    scanf("%d", &operation);

    return operation;
}
float getFirstOperand()
{
    float operand = 0;
    printf("Please insert the First operand: ");
    scanf("%f", &operand);

    return operand;
}
float getSecondOperand()
{
    float operand = 0;
    printf("Please insert the Second operand: ");
    scanf("%f", &operand);

    return operand;
}
void addition(float firstOperand, float secondOperand)
{
    printf("%f + %f = %.2f\n", firstOperand, secondOperand, firstOperand + secondOperand);
}

void subtraction(float firstOperand, float secondOperand)
{
    printf("%f - %f = %.2f\n", firstOperand, secondOperand, firstOperand - secondOperand);
}

void multiplication(float firstOperand, float secondOperand)
{
    printf("%f * %f = %.2f\n", firstOperand, secondOperand, firstOperand * secondOperand);
}

void division(float firstOperand, float secondOperand)
{
    if (secondOperand != 0)
    {
        printf("%f / %f = %f\n", firstOperand, secondOperand, firstOperand / secondOperand);
    }
    else
    {
        printf("invalid second Operand: Can't devide with zero\n");
    }
}