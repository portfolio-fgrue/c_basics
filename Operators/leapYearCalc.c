#include <stdio.h>

int main()
{
    int year = 0;
    printf("Please enter a Year to check if it's a leap year: ");
    scanf("%d", &year);

    if ((year % 4) == 0 && !(year % 100) == 0)
    {
        printf("This year is a leap year: %d", year);
    }
    else if ((year % 100) == 0 && !(year % 400) == 0)
    {
        printf("This year is a leap year: %d", year);
    }
    else if ((year % 400) == 0)
    {
        printf("This year is a leap year: %d", year);
    }
    else
        printf("This year is not a leap year: %d", year);

    return 0;
}
